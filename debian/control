Source: qtav
Maintainer: Debian Qt extras Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Steve M. Robbins <smr@debian.org>
Section: video
Priority: optional
Build-Depends: debhelper (>= 11~),
               libass-dev,
               libavcodec-dev,
               libavdevice-dev,
               libavfilter-dev,
               libavformat-dev,
               libswresample-dev,
               libavutil-dev,
               libegl1-mesa-dev,
               libopenal-dev,
               libpulse-dev,
               libqt5opengl5-dev,
               libqt5svg5-dev,
               libqt5x11extras5-dev,
               libswscale-dev,
               libuchardet-dev,
               libva-dev [!hurd-any],
               libxv-dev,
               qt5-qmake,
               qtdeclarative5-dev
Standards-Version: 4.1.5
Homepage: http://qtav.org
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/qtav
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/qtav.git

Package: libqtav1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: QtAV library
 QtAV is a multimedia playback library based on Qt and FFmpeg.
 It can help you to write a player with less effort than ever before.
 .
 This package contains the QtAV library.

Package: libqtavwidgets1
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: QtAV Widgets module
 QtAV is a multimedia playback library based on Qt and FFmpeg.
 It can help you to write a player with less effort than ever before.
 .
  This package contains a set of widgets to play media.

Package: libqtav-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libqt5opengl5-dev,
         libqtav1 (= ${binary:Version}),
         libqtavwidgets1 (= ${binary:Version}),
         ${misc:Depends}
Description: QtAV development files
 QtAV is a multimedia playback framework based on Qt and FFmpeg.
 It can help you to write a player with less effort than ever before.
 .
 This package contains the header development files for building some
 QtAV applications using QtAV headers.

Package: libqtav-private-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libqtav-dev (= ${binary:Version}), ${misc:Depends}
Description: QtAV private development files
 QtAV is a multimedia playback library based on Qt and FFmpeg.
 It can help you to write a player with less effort than ever before.
 .
 This package contains the private header development files for building some
 QtAV applications using QtAV private headers.
 .
 Use at your own risk.

Package: qml-module-qtav
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: QtAV QML module
 QtAV is a multimedia playback library based on Qt and FFmpeg.
 It can help you to write a player with less effort than ever before.
 .
 This package contains the QtAV QML module for Qt declarative.

Package: qtav-players
Architecture: any
Multi-Arch: no
Depends: qml-module-qt-labs-folderlistmodel,
         qml-module-qtav,
         qml-module-qtquick-controls,
         qml-module-qtquick-dialogs,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: QtAV/QML players
 QtAV is a multimedia playback framework based on Qt and FFmpeg.
 High performance. User & developer friendly.
 .
 This package contains the QtAV based players
